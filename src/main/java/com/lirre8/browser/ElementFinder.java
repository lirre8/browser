package com.lirre8.browser;

import com.lirre8.browser.globalmatch.GlobalMatchException;
import com.lirre8.browser.globalmatch.GlobalMatchResult;
import org.apache.commons.lang3.Validate;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by sebastian on 1/1/16.
 *
 *
 */
abstract class ElementFinder {

    private static final Logger LOG = Logger.getAnonymousLogger();

    protected static Map<GlobalMatchResult, Set<XPathMatcher>> globalResultMatches = new HashMap<>();

    // Default values
    static               int     defaultMaxWait       = 10;
    private static final boolean defaultVisibleOnly   = true;
    private static final int     defaultExistsMaxWait = 0;

    protected ElementFinder() {}

    protected abstract SearchContext getSearchContext();
    protected abstract Element getRootElement();

    public Builder find() {
        return new Builder();
    }

    public <T> WaitForAny<T> waitForAny(@SuppressWarnings("unused") Class<T> clazz) {
        return new WaitForAny<>(this);
    }

    public ExecuteMatchingCase executeMatchingCase() {
        return new ExecuteMatchingCase(this);
    }

    private Element getElement(final XPathMatcher findMatcher, int maxWait) {
        return getElementFromAny(Collections.singleton(findMatcher), maxWait);
    }

    private List<Element> getElements(final XPathMatcher findMatcher, int maxWait) {
        return getElementsFromAny(Collections.singleton(findMatcher), maxWait);
    }

    private boolean elementExists(final XPathMatcher findMatcher, int maxWait) {
        return getElementIfExists(findMatcher, maxWait) != null;
    }

    private Element getElementIfExists(final XPathMatcher findMatcher, int maxWait) {
        List<Element> foundElementList = getElementsIfExists(findMatcher, maxWait);
        if (foundElementList.isEmpty()) {
            return null;
        }
        return foundElementList.get(0);
    }

    private List<Element> getElementsIfExists(final XPathMatcher findMatcher, int maxWait) {
        return findElements(Collections.singleton(findMatcher), maxWait);
    }

    Element getElementFromAny(final Set<XPathMatcher> findMatcherList, int maxWait) {
        return getElementsFromAny(findMatcherList, maxWait).get(0);
    }

    private List<Element> getElementsFromAny(final Set<XPathMatcher> findMatcherList, int maxWait) {
        List<Element> foundElementList = findElements(findMatcherList, maxWait);
        if (foundElementList.isEmpty()) {
            throw getNoElementsFoundException(findMatcherList, maxWait);
        }
        return foundElementList;
    }

    private List<Element> findElements(final Set<XPathMatcher> xPathMatchers, int maxWait) {
        Validate.isTrue(maxWait >= 0);

        final Set<XPathMatcher> globalMatches = globalResultMatches.values().stream().flatMap(Set::stream).collect(Collectors.toSet());

        FindElementsResult result = null;

        long currentSearchTime   = System.currentTimeMillis();
        final long endSearchTime = currentSearchTime + maxWait*1000;

        while (endSearchTime >= currentSearchTime) {
            // Try to find a match using the current SearchContext and XPathMatchers
            result = findElements(getSearchContext(), xPathMatchers);
            if (result.foundMatch()) {
                break;
            }
            // Try to find a match using the root SearchContext and the global XPathMatchers
            result = findElements(Browser.webDriver, globalMatches);
            if (result.foundMatch()) {
                throw new GlobalMatchException(getGlobalMatchResult(result.matchingXPath));
            }

            sleep(250);
            currentSearchTime = System.currentTimeMillis();
        }

        assert result != null;
        logFindResult(xPathMatchers, result.foundWebElements, result.matchingXPath);

        return webElementsToElements(result.foundWebElements, result.matchingXPath);
    }

    private static FindElementsResult findElements(SearchContext searchContext, Set<XPathMatcher> xPathMatchers) {
        FindElementsResult result = new FindElementsResult(null, null);

        for (XPathMatcher xPathMatcher : xPathMatchers) {
            result = findElements(searchContext, xPathMatcher);
            if (result.foundMatch()) {
                break;
            }
        }

        return result;
    }

    private static FindElementsResult findElements(SearchContext searchContext, XPathMatcher xPathMatcher) {
        String matchingXpath = null;

        List<WebElement> foundWebElements = searchContext.findElements(By.xpath(xPathMatcher.getXPath()));
        if (!foundWebElements.isEmpty()) {
            if (xPathMatcher.isVisibleOnly()) {
                foundWebElements = filterNonVisibleElements(foundWebElements);
            }
            if (!foundWebElements.isEmpty()) {
                matchingXpath = xPathMatcher.getXPath();
            }
        }

        return new FindElementsResult(foundWebElements, matchingXpath);
    }

    private static GlobalMatchResult getGlobalMatchResult(String xPath) {
        Optional<GlobalMatchResult> result = globalResultMatches.keySet().stream()
                .filter(r -> globalResultMatches.get(r).stream().anyMatch(xPathMatcher -> xPathMatcher.getXPath().equals(xPath)))
                .findFirst();
        return result.orElseThrow(() -> new RuntimeException("WTF! GlobalMatch: Element was found but xpath didn't match any of the global match cases!"));
    }

    private void logFindResult(Set<XPathMatcher> xPathMatchers, List<WebElement> foundWebElements, String matchingXPath) {
        final String xPathSuffix;
        final String elementSuffix;
        final String xPathsText;
        int nbrFoundElements = 0;

        if (matchingXPath == null) {
            final StringJoiner xPathJoiner = new StringJoiner("\", \"");
            xPathMatchers.stream().forEach(xPathMatcher -> xPathJoiner.add(xPathMatcher.getXPath()));
            xPathsText  = xPathJoiner.toString();
            xPathSuffix = xPathMatchers.size() > 1 ? "s" : "";
        }
        else {
            nbrFoundElements = foundWebElements.size();
            xPathsText       = matchingXPath;
            xPathSuffix      = "";
        }

        elementSuffix = nbrFoundElements != 1 ? "s" : "";

        LOG.log(Level.INFO, "Found {0} element{1} matching the xpath{2} \"{3}\" {4}.",
                new Object[]{nbrFoundElements, elementSuffix, xPathSuffix, xPathsText, getRootText()});
    }

    private NoElementsFoundException getNoElementsFoundException(final Set<XPathMatcher> xPathMatchers, final int maxWait) {
        final String maxWaitSuffix     = maxWait != 1 ? "s" : "";
        final String xPathSuffix       = xPathMatchers.size() > 1 ? "s" : "";
        final StringJoiner xPathJoiner = new StringJoiner("\", \"");

        xPathMatchers.stream().forEach(xPathMatcher -> xPathJoiner.add(xPathMatcher.getXPath()));

        return new NoElementsFoundException(String.format("No elements found using xpath%s: \"%s\". Searched %s for %s second%s.", xPathSuffix, xPathJoiner.toString(), getRootText(), maxWait, maxWaitSuffix));
    }

    private String getRootText() {
        final Element rootElement = getRootElement();
        if (rootElement != null) {
            return "from root " + rootElement.toString();
        }
        return "in the whole browser";
    }

    private List<Element> webElementsToElements(final List<WebElement> webElements, final String xpath) {
        final List<Element> elements = new ArrayList<>();

        if (xpath != null) {
            for (int i = 0; i < webElements.size(); i++) {
                elements.add(new Element(webElements.get(i), xpath, i + 1, getRootElement()));
            }
        }

        return elements;
    }

    private static List<WebElement> filterNonVisibleElements(final List<WebElement> elements) {
        final List<WebElement> visibleElements = new ArrayList<>();

        elements.stream().filter(WebElement::isDisplayed).forEach(visibleElements::add);

        return visibleElements;
    }

    private static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            LOG.warning("Sleep failed: " + e);
        }
    }

    private static class FindElementsResult {
        private List<WebElement> foundWebElements;
        private String           matchingXPath;

        private FindElementsResult(List<WebElement> foundWebElements, String matchingXPath) {
            this.foundWebElements = foundWebElements;
            this.matchingXPath = matchingXPath;
        }

        private boolean foundMatch() {
            return matchingXPath != null;
        }
    }

    private static class NoElementsFoundException extends RuntimeException {

        public NoElementsFoundException(String message) {
            super(message);
        }
    }

    public class Builder {
        private String  xPath;
        private boolean visibleOnly;
        private int     maxWait;
        private int     existsMaxWait;

        private Builder() {
            // Set default values
            visibleOnly   = defaultVisibleOnly;
            maxWait       = defaultMaxWait;
            existsMaxWait = defaultExistsMaxWait;
        }

        public Builder xPath(String xPath) {
            this.xPath = xPath;
            return this;
        }

        public Builder allowHidden() {
            visibleOnly = false;
            return this;
        }

        public Builder maxWait(int seconds) {
            maxWait       = seconds;
            existsMaxWait = seconds;
            return this;
        }

        public Element getElement() {
            return ElementFinder.this.getElement(new XPathMatcher(xPath, visibleOnly), maxWait);
        }

        public List<Element> getElements() {
            return ElementFinder.this.getElements(new XPathMatcher(xPath, visibleOnly), maxWait);
        }

        public boolean elementExists() {
            return ElementFinder.this.elementExists(new XPathMatcher(xPath, visibleOnly), existsMaxWait);
        }

        public Element getElementIfExists() {
            return ElementFinder.this.getElementIfExists(new XPathMatcher(xPath, visibleOnly), existsMaxWait);
        }

        public List<Element> getElementsIfExists() {
            return ElementFinder.this.getElementsIfExists(new XPathMatcher(xPath, visibleOnly), existsMaxWait);
        }
    }
}
