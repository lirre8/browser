package com.lirre8.browser;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by sebastian on 4/9/16.
 *
 */
public class WaitForAny<T> {

    private final ElementFinder elementFinder;

    private final Map<T, Set<XPathMatcher>> results;
    private int maxWait;

    WaitForAny(ElementFinder elementFinder) {
        results = new HashMap<>();
        this.elementFinder = elementFinder;
        // Set default values
        maxWait = ElementFinder.defaultMaxWait;
    }

    public WaitForAny<T> add(Result<T> result) {
        final Set<XPathMatcher> matcherSet;
        if (results.containsKey(result.result)) {
            matcherSet = results.get(result.result);
        }
        else {
            matcherSet = new HashSet<>();
            results.put(result.result, matcherSet);
        }

        matcherSet.addAll(result.get());

        return this;
    }

    public WaitForAny<T> maxWait(int seconds) {
        maxWait = seconds;
        return this;
    }

    public T getResult() {
        final Set<XPathMatcher> allXPathMatchers = results.values().stream().flatMap(Set::stream).collect(Collectors.toSet());

        final Element findResult = elementFinder.getElementFromAny(allXPathMatchers, maxWait);

        return getResultFromElement(findResult);
    }

    private T getResultFromElement(Element element) {
        return results.keySet().stream()
                .filter(r -> results.get(r).stream().anyMatch(xPathMatcher -> xPathMatcher.getXPath().equals(element.getXPath())))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("WTF! WaitForAny: Element was found but xpath didn't match any of the WaitForAny's cases!"));
    }
}
