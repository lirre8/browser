package com.lirre8.browser;

/**
 * Created by sebastian on 4/10/16.
 *
 */
class XPathMatcher implements Comparable<XPathMatcher> {

    private final String  xPath;
    private final boolean visibleOnly;

    XPathMatcher(String xPath, boolean visibleOnly) {
        this.xPath       = xPath;
        this.visibleOnly = visibleOnly;
    }

    boolean isVisibleOnly() {
        return visibleOnly;
    }

    String getXPath() {
        return xPath;
    }

    @Override
    public int compareTo(@SuppressWarnings("NullableProblems") XPathMatcher that) {
        return this.xPath.compareTo(that.xPath);
    }
}
