package com.lirre8.browser;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.Locatable;
import org.sikuli.api.DefaultScreenLocation;
import org.sikuli.api.DefaultScreenRegion;
import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ScreenLocation;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sebastian on 12/20/15.
 *
 *
 */
public class Element extends ElementFinder {

    private static final Logger LOG = Logger.getAnonymousLogger();

    private static final ScreenRegion screen   = new DesktopScreenRegion();
    private static final Mouse        mouse    = new DesktopMouse();
    private static final Keyboard     keyboard = new DesktopKeyboard();

    private final WebElement webElement;
    private final String     xpath;
    private final Element    rootElement;
    private final int        index;

    public Element(final WebElement webElement, final String xpath, final int index, final Element rootElement) {
        this.webElement  = webElement;
        this.xpath       = xpath;
        this.rootElement = rootElement;
        this.index       = index;
    }

    public void click() {
        LOG.log(Level.INFO, "Clicking {0}.", toString());
        webElement.click();
    }

    public String getAttribute(final String name) {
        return webElement.getAttribute(name);
    }

    public String getText() {
        return webElement.getText().trim();
    }

    public void setText(String text) {
        enterText(text, true);
    }

    public void appendText(String text) {
        enterText(text, false);
    }

    private void enterText(String text, boolean clear) {
        LOG.log(Level.INFO, "Enter text of {0}.", toString());
        if (clear) {
            webElement.clear();
        }
        webElement.sendKeys(text);
    }

    public void setSelected() {
        if (!webElement.isSelected()) {
            webElement.click();
        }
    }

    public void setUnselected() {
        if (webElement.isSelected()) {
            webElement.click();
        }
    }

    public boolean isSelected() {
        return webElement.isSelected();
    }

    public void mouseClick() {
        LOG.log(Level.INFO, "Mouse clicking {0}.", toString());
        Browser.actions.click(webElement).perform();
    }

    public void keyboardType(String text) {
        Browser.actions.sendKeys(webElement, text).perform();
    }


    public void sikuliClick() {
        LOG.log(Level.INFO, "Sikuli clicking {0}.", toString());
        Point point = getCenterLocationOnScreen();
        ScreenLocation location = new DefaultScreenLocation(screen.getScreen(), point.x, point.y);
        mouse.click(location);
    }

    public void sikuliType(String text) {
        sikuliClick();
        LOG.info("Sikuli typing.");
        keyboard.type(text);
    }

    private Point getViewPortCoordinates() {
        org.openqa.selenium.Point point = null;
        for (int i = 0; i < 3; i++) {
            point = ((Locatable) webElement).getCoordinates().inViewPort();
            LOG.info("X: " + point.x + " Y: " + point.y);
        }
        return new Point(point.x, point.y);
    }

    private Dimension getSize() {
        org.openqa.selenium.Dimension dimension = webElement.getSize();
        return new Dimension(dimension.width, dimension.height);
    }

    private Point getCenterLocationOnScreen() {
        Point     location = getViewPortCoordinates();
        Dimension size     = getSize();
        return new Point(location.x + size.width/2, location.y + size.height/2);
    }

    @Override
    public String toString() {
        return String.format("element %s with xpath \"%s\"" + (rootElement != null ? ", and root element:\n" + rootElement : ""), index, xpath);
    }

    @Override
    protected Element getRootElement() {
        return this;
    }

    @Override
    protected SearchContext getSearchContext() {
        return webElement;
    }

    String getXPath() {
        return xpath;
    }
}
