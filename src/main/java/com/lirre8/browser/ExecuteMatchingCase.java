package com.lirre8.browser;

import java.util.HashSet;
import java.util.Set;

public class ExecuteMatchingCase {

    private final ElementFinder     elementFinder;
    private final Set<XPathMatcher> xPathMatchers;

    private int maxWait;

    ExecuteMatchingCase(ElementFinder elementFinder) {
        this.elementFinder = elementFinder;
        xPathMatchers = new HashSet<>();
        maxWait = ElementFinder.defaultMaxWait;
    }

    public ExecuteMatchingCase add(Case c) {
        xPathMatchers.addAll(c.getXPathMatchers());
        return this;
    }

    public ExecuteMatchingCase maxWait(int seconds) {
        maxWait = seconds;
        return this;
    }

    public void match() {
        Element element = elementFinder.getElementFromAny(xPathMatchers, maxWait);
    }
}
