package com.lirre8.browser;

import com.lirre8.browser.globalmatch.GlobalMatchResult;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by sebastian on 12/20/15.
 *
 *
 */
public class Browser extends ElementFinder {

    private static final Logger LOG = Logger.getAnonymousLogger();

    protected static WebDriver webDriver = null;
    protected static Actions   actions   = null;

    private static Browser instance = null;

    private Browser() {}

    public static synchronized Browser getInstance() {
        if (instance == null) {
            instance = new Browser();
        }
        return instance;
    }

    public void startBrowser() {
        if (webDriver != null) {
            throw new RuntimeException("Browser already started!");
        }
        LOG.info("Starting the browser");
        webDriver = new FirefoxDriver();
        actions   = new Actions(webDriver);
    }

    public void quit() {
        webDriver.quit();
    }

    public void loadPage(final String url) {
        LOG.info("Loading URL: " + url);
        webDriver.get(url);
    }

    public String getCurrentUrl() {
        return webDriver.getCurrentUrl();
    }

    public Object executeScript(String javascript) {
        return executeScript(javascript, new Object[0]);
    }

    public Object executeScript(String javascript, Object... objects) {
        final JavascriptExecutor je = (JavascriptExecutor) webDriver;
        return je.executeScript(javascript, objects);
    }

    public void setDefaultMaxWait(int seconds) {
        defaultMaxWait = seconds;
    }

    public int getDefaultMaxWait() {
        return defaultMaxWait;
    }

    @SafeVarargs
    public final void setGlobalMatches(Result<? extends GlobalMatchResult>... globalResults) {
        globalResultMatches = new HashMap<>();
        addGlobalMatches(globalResults);
    }

    @SafeVarargs
    public final void addGlobalMatches(Result<? extends GlobalMatchResult>... globalResults) {
        for (Result<? extends GlobalMatchResult> result : globalResults) {
            final Set<XPathMatcher> matcherSet;
            if (globalResultMatches.containsKey(result.result)) {
                matcherSet = globalResultMatches.get(result.result);
            }
            else {
                matcherSet = new HashSet<>();
                globalResultMatches.put(result.result, matcherSet);
            }
            matcherSet.addAll(result.get());
        }
    }

    @Override
    protected Element getRootElement() {
        return null;
    }

    @Override
    protected SearchContext getSearchContext() {
        return webDriver;
    }
}
