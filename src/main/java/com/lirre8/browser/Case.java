package com.lirre8.browser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Case {

    private Executor    executor;
    private Set<String> xPaths;
    private boolean     visibleOnly;

    public static Case execute(Executor executor) {
        return new Case(executor);
    }

    private Case(Executor executor) {
        this.executor = executor;
        xPaths = new HashSet<>();
    }

    public Case forXPath(String xPath) {
        return forXPaths(xPath);
    }

    public Case forXPaths(String... xPaths) {
        this.xPaths.addAll(Arrays.asList(xPaths));
        return this;
    }

    public Case allowHidden() {
        visibleOnly = false;
        return this;
    }

    Set<XPathMatcher> getXPathMatchers() {
        return xPaths.stream()
                .map(xPath -> new XPathMatcher(xPath, visibleOnly))
                .collect(Collectors.toSet());
    }

    void execute() {
        executor.execute();
    }

    @FunctionalInterface
    public interface Executor {
        void execute();
    }
}
