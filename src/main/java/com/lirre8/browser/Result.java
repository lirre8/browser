package com.lirre8.browser;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Result<T> {

    final T                   result;
    private final Set<String> xPaths;
    private boolean           visibleOnly;

    public Result(T result) {
        this.result = result;
        xPaths = new HashSet<>();
        // Set default values
        visibleOnly = true;
    }

    public static <T> Result<T> of(T result) {
        return new Result<>(result);
    }

    public Result<T> set() {
        return this;
    }

    public Result<T> xPath(String xPath) {
        xPaths.add(xPath);
        return this;
    }

    public Result<T> allowHidden() {
        visibleOnly = false;
        return this;
    }

    Set<XPathMatcher> get() {
        if (xPaths.isEmpty()) {
            throw new RuntimeException("A result was created without any matching cases!");
        }

        return xPaths.stream()
                .map(xPath -> new XPathMatcher(xPath, visibleOnly))
                .collect(Collectors.toSet());
    }
}
