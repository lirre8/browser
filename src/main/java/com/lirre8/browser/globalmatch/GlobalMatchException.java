package com.lirre8.browser.globalmatch;

public class GlobalMatchException extends RuntimeException {

    private GlobalMatchResult result;

    public GlobalMatchException(GlobalMatchResult result) {
        this.result = result;
    }

    public GlobalMatchResult getResult() {
        return result;
    }
}
